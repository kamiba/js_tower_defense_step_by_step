//游戏数据控制类
var Game = {
    //图片列表信息
    imgList : {},
    //画布列表信息
    canvasList : {},

    //初始化
    init : function(){
        this.initImg();
        this.initCanvas();
    },
    //初始化图片
    initImg : function(){

        this.imgList = {

            enemy_img : document.getElementById("enemy_img"),
            tower_img : document.getElementById("tower_img"),
            bullet_img : document.getElementById("bullet_img"),
            btn_img : document.getElementById("btn_img")
        }
    },
    //初始化画布
    initCanvas : function(){

        this.canvasList = {

            map : document.getElementById("map").getContext("2d"),
            main : document.getElementById("main").getContext("2d"),
            info : document.getElementById("info").getContext("2d"),
            select : document.getElementById("select").getContext("2d"),
            tower : document.getElementById("tower").getContext("2d")
        }
    },

    //开始
    start : function(){

        switch(document.getElementById("select_map").value){
            case "1":
                MapData = MapOne;
                break;
            case "2":
                MapData = MapTwo;
                break;
            default:
                MapData = MapOne;
                break;
        }
        Map.draw(this.canvasList.map);
        this.timer = setInterval(Game.loop,20);
    },
    //循环体
    loop : function(){
        Canvas.clear(Game.canvasList.main,500,500);
    }
}

Game.init();