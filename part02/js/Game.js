//游戏数据控制类
var Game = {
    //图片列表信息
    imgList : {},
    //画布列表信息
    canvasList : {},
    //敌人类表
    enemyList : [],
    //关卡数
    mission : 0,
    //每个敌人的出场间隔时间
    enemyTime : 1000,
    //每个敌人的出场间隔延迟
    enemyLazy : 0,
    //计时器ID
    timer : 0,
    //初始化
    init : function(){
        this.initImg();
        this.initCanvas();
    },
    //初始化图片
    initImg : function(){

        this.imgList = {

            enemy_img : document.getElementById("enemy_img"),
            tower_img : document.getElementById("tower_img"),
            bullet_img : document.getElementById("bullet_img"),
            btn_img : document.getElementById("btn_img")
        }
    },
    //初始化画布
    initCanvas : function(){

        this.canvasList = {

            map : document.getElementById("map").getContext("2d"),
            main : document.getElementById("main").getContext("2d"),
            info : document.getElementById("info").getContext("2d"),
            select : document.getElementById("select").getContext("2d"),
            tower : document.getElementById("tower").getContext("2d")
        }
    },
    initEnemy : function(){
        if(Game.enemyLazy > 0){

            Game.enemyLazy -= 20;

            return false;
        }
        else{

            Game.enemyLazy = Game.enemyTime;
        }
        //新增一个敌人
        var enemy = new Enemy(Game.canvasList.main,Game.imgList.enemy_img,Game.mission,55,0,40,40);
        Game.enemyList.push(enemy);
    },
    //开始
    start : function(){

        switch(document.getElementById("select_map").value){
            case "1":
                MapData = MapOne;
                break;
            case "2":
                MapData = MapTwo;
                break;
            default:
                MapData = MapOne;
                break;
        }
        Map.draw(this.canvasList.map);
        this.timer = setInterval(Game.loop,20);
    },
    //循环体
    loop : function(){
        Canvas.clear(Game.canvasList.main,500,500);

        Game.initEnemy();

        drawEnemy();

        updateEnemy();
    }
}

Game.init();