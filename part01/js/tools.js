//画布类
var Canvas = {
    //清除画布
    clear : function(cxt,x,y){
        cxt.clearRect(0,0,x,y);
    },
    clearRect : function(cxt,x,y,width,height){
        cxt.clearRect(x,y,width,height);
    },

    //画填充的方
    fillRect : function(cxt,x,y,width,height,color){

        cxt.fillStyle = color;
        cxt.fillRect(x,y,width,height);
    },
    //画边框的方
    drawRect : function(cxt,x,y,width,height,color){

        cxt.strokeStyle = color;
        cxt.lineWidth = 1;
        cxt.strokeRect(x,y,width,height);
    }

}

